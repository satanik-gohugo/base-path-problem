FROM registry.gitlab.com/pages/hugo/hugo_extended:0.76.5 as builder

ARG DUMMY_URL=http://dummy-url.tld

WORKDIR /site

COPY . .

RUN \
  HUGO_ENV=production \
  hugo \
    --baseURL ${DUMMY_URL} \
    --cleanDestinationDir \
    --debug \
    --environment production \
    --log \
    --minify \
    --templateMetrics \
    --templateMetricsHints \
    --verbose \
    --verboseLog

FROM nginx:1.19.3-alpine

ARG DUMMY_URL=http://dummy-url.tld

RUN apk add --no-cache openssl=1.1.1g-r0

COPY --from=builder /site/public /usr/share/nginx/html
COPY shell/entrypoint.sh /

ENV DUMMY_URL=${DUMMY_URL}
ENV CONTEXT_URL=/
ENV TITLE=Minimal
ENV DESCRIPTION="Long Description"

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
