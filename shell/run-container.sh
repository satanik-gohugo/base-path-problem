#!/usr/bin/env bash
docker run --rm -it \
  --env-file .env \
  -p 8013:80 $CONTAINER "$@"
